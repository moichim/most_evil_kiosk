# The Most Evil Kiosk

[2022-01-20] It works!

## Aim

A two-display multimedia player playing images, videos and/or texts from ~/storage/.

## Development

Install last LTS version of [Node.js](https://nodejs.org/en/). It is recommended to use [NVM](https://github.com/nvm-sh/nvm) for management of Node.js versions.

Clone the repository & go to the folder.

Install dependencies by `npm install` or `yarn`.

Start development environment:

- `npm run start` or `yarn start` 
- This watches src/ folder and updates changes
- Updates are applied only in renderer context. Any changes in main process require manual restart of the development environment.

Build an executable for the current platform:

- `npm run package` or `yarn package` 
- Packages the app in dist/ folder.

Other commands:

See [Electron Forge Documentation](https://www.electronforge.io/cli#commands) for make and lint commands.

## Todo

- regroup files into the structure below (including `package.json`)
- set up screens & windows
- implement time limits into projector + automatical triggering of the screensaver
- implement the desired IPC messages
- test playback of large videos
- test building for Rapsberry

### Desired file structure

```
src/
    main.js
    Main/
        Storage.js
        VLC.js
    Controller/
        Screens/
            ---
        Utils/
            Tablet.js      <==== the main application in vanilla js
        controller.js
        controller.html
        controller.scss
    Projector/
        Screens/
            ...
        Utils/
            Reciever.js     <=== Projector.js
        projector.js
        projector.html
        projector.scss

```

### Desired messaging

```
Controller          Main                Projector
----------          ----                ---------

request-playback >  order-playback  >   []

[]               <  respond-end     <   report-ended

```