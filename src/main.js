const { 
  app, 
  BrowserWindow, 
  ipcMain, 
  protocol, 
  session,
  screen
} = require('electron');

const path = require('path');

import VLC from "./Main/VLC";
import Storage from "./Main/Storage";

// Handle creating/removing shortcuts on Windows when installing/uninstalling.
if (require('electron-squirrel-startup')) {
  // eslint-disable-line global-require
  app.quit();
}

const createWindow = () => {
  // Create the browser window.
  const mainWindow = new BrowserWindow({
    kiosk: false,
    webPreferences: {
      nodeIntegration: true,
      enableRemoteModule: true,
      contextIsolation: false,
      nativeWindowOpen: true
    }
  });

  // require('@electron/remote/main').initialize();

  // and load the index.html of the app.
  mainWindow.loadURL(MAIN_WINDOW_WEBPACK_ENTRY);

  // Open the DevTools.
  mainWindow.webContents.openDevTools();
};

// This method will be called when Electron has finished
// initialization and is ready to create browser windows.
// Some APIs can only be used after this event occurs.
// app.on('ready', createWindow);

// Quit when all windows are closed, except on macOS. There, it's common
// for applications and their menu bar to stay active until the user quits
// explicitly with Cmd + Q.
app.on('window-all-closed', () => {
  if (process.platform !== 'darwin') {
    app.quit();
  }
});

app.on('activate', () => {
  // On OS X it's common to re-create a window in the app when the
  // dock icon is clicked and there are no other windows open.
  if (BrowserWindow.getAllWindows().length === 0) {
    createWindow();
  }
});

// In this file you can include the rest of your app's specific main process
// code. You can also put them in separate files and import them here.






// My code


let controller;
let projector;

/**
 * Main application bootstrap.
 */
const bootWindows = () => {

  console.log( screen.getAllDisplays() );

  // Controller

  controller = new BrowserWindow({
    title: "Controller",
    kiosk: false,
    webPreferences: {
      nodeIntegration: true,
      contextIsolation: false,
      webSecurity: false
    }
  });

  controller.loadURL( CONTROLLER_WEBPACK_ENTRY );
  controller.webContents.openDevTools();

  controller.on( "closed", function () {
    controller = null;
  } );

  // Projector

  projector = new BrowserWindow({
    title: "Projector",
    kiosk: false,
    webPreferences: {
      nodeIntegration: true,
      contextIsolation: false,
      webSecurity: false
    },
    parent: controller,
    x: 1920,
    y: 400
  });

  projector.loadURL( PROJECTOR_WEBPACK_ENTRY );
  projector.webContents.openDevTools();

  projector.on( "closed", function() {
    projector = null;
  } );

}

/**
 * Boot both windows when the app is ready.
 */
app.on('ready', bootWindows);


/**
 * Enable media access through storage://
 * 
 * Each renderer process must request their media through storage:// protocol. The path must be absolute which is done in Main\Storage.
 *  
 * @todo Use process.env.HOME for loading from relative URLs from ~/storage/ Example: storage://bloud.jpg should forward to /home/pi/storage/bloud.jpg. 
 * 
 * @link https://github.com/electron/electron/issues/23757#issuecomment-640146333
 */
app.whenReady().then(() => {

  /** 
   * Register storage:/// protocol for access to local filesystem
   */
  protocol.registerFileProtocol('storage', (request, callback) => {
    const url = request.url.substr(10)
    callback(decodeURI(path.normalize(url)))
  });

  /**
   * Enable storage:/// in src="" atributes
   * 
   * @link https://www.electronjs.org/docs/latest/tutorial/security#7-define-a-content-security-policy
   * @link https://developer.mozilla.org/en-US/docs/Web/HTTP/Headers/Content-Security-Policy
   */
  session.defaultSession.webRequest.onHeadersReceived((details, callback) => {
    callback({
      responseHeaders: {
        ...details.responseHeaders,
        'Content-Security-Policy': [
          "img-src storage: filesystem: 'unsafe-eval' 'unsafe-inline' 'self'",
          "media-src storage: filesystem: 'unsafe-eval' 'unsafe-inline' 'self'"
        ]
      }
    })
  })

});



// Route a controller request to the projector
ipcMain.on( "main-request", ( event, arg ) => {

  // If the incomming arguments contain the key "filename", transform it into absolute file:// URL

  if ( "file" in arg ) {
    arg.file = "storage:///" + Storage.getAbsolutePath( arg.file );
  }

  projector.webContents.send( "projector-request", arg );
} );




// Launch VLC on over the projector window
ipcMain.on( "launch-vlc", ( event, arg ) => {

  VLC.play( "labir_edu_720.mp4", function(){
    controller.webContents.send( "terminated-vlc", "Tak už je to za námi." );
  } );

} );