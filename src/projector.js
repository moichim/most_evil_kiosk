import Projector from "./Renderer/Projector";

import "./Renderer/Screens/Screen.scss";


console.log( '👋 Projector is starting.' );



/**
 * Instantiate the projector controller
 * @see projector.html
 */
const projector = new Projector;



// Recieve signals from the main process
const { ipcRenderer } = window.require( "electron" );

// Listen to incoming projector requests
ipcRenderer.on( "projector-request", ( event, arg ) => {
    // console.log( arg );
    projector.recieve( arg );
} );


