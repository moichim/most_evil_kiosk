import Image from "../Renderer/Screens/Image";
import Screensaver from "../Renderer/Screens/Screensaver";
import Text from "../Renderer/Screens/Text";
import Video from "../Renderer/Screens/Video";


class ProjectorRequest {

    static types = [
        Image.key,
        Screensaver.key,
        Text.key,
        Video.key
    ];


    /**
     * 
     * @param {string} screen_class 
     * @param {Object} data 
     */
    constructor( screen_slug, data ) {

        if ( ! this.isValidScreen( screen_slug ) ) {
            throw new ReferenceError( "Invalid screen!" );
        }


        this.screen = this.getClassFromSlug( screen_slug );

        this.setData( data );

    }


    getScreenKey() {

        return this.screen.key;
    }


    /**
     * Route this.screen property based on slug
     * @param {string} screen_slug 
     * @returns 
     */
    getClassFromSlug( screen_slug ) {
        if ( screen_slug === Image.key ) {
            return Image;
        } else if ( screen_slug === Video.key ) {
            return Video;
        } else if ( screen_slug === Text.key ) {
            return Text;
        } else if ( screen_slug === Screensaver.key ) {
            return Screensaver;
        }
    }


    /**
     * 
     * @param {Object} data 
     */
    setData( data ) {
        this.data = Object.assign( this.screen.defaults, data );
    }

    /**
     * 
     * @returns  {Object}
     */
    getData() {
        return this.data;
    }


    /**
     * 
     * @param {string} screen 
     * @returns 
     */
    isValidScreen( screen ) {
        return ProjectorRequest.types.includes( screen );
    }


}

export default ProjectorRequest;