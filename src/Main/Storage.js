const path = require( "path" );

/**
 * A static class for local storage lookup
 */
class Storage {

    /**
     * Look for local path within the ~/storage/ folder
     * @param {string} relative_path 
     * @returns {string}
     */
    static getAbsolutePath( relative_path ) {
        return path.join(
            process.env.HOME,
            "storage",
            relative_path
        );
    }

}

export default Storage;