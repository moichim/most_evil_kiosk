const child_process = require( "child_process" );
const path = require( "path" );

import Storage from "./Storage";

class VLC {
    
    static getExecutalbe() {
        
        // Windows 
        if ( process.platform === "win32" ) {
            return path.join( "C:", "Program Files", "VideoLAN", "VLC", "vlc.exe" );
        }
        
        // Linux
        else if ( process.platform === "linux" ) {
            return "/usr/bin/vlc";
        }

        // MacOS
        else if ( process.platform === "darwin" ) {
            return "/Applications/VLC.app/Contents/MacOS/VLC";
        }

        throw new ReferenceError( "VLC executable not found." );

    }

    static getBaseParameters() {
        return [
            "-f",
            "--one-instance",
            "--intf=dummy",
            // "--no-loop",
            // "--no-repeat",
            // "--no-playlist-autostart",
            // "--no-playlist-enqueue",
            "--play-and-exit",
            "--no-video-title",
            "--no-embedded-video",
            // "--directx-device=\\\\.\\DISPLAY2",
        ];
    }

    static getParameters( filename ) {
        return this.getBaseParameters().concat( [ Storage.getAbsolutePath( filename ) ] );
    }

    static play( filename, exit_callback ) {

        let proc = child_process.spawn( this.getExecutalbe(), this.getParameters( filename ) );

        proc.stderr.on( "data", (data) => {
            console.error( "VLC stderr", data.toString() );
        } );

        proc.stdout.on( "data", (data) => {
            console.log( "VLC stdout", data.toString() );
        } );

        proc.on( "exit", (code, signal) => {
            exit_callback();
            console.log( "VLC exited with code " + code, signal );
        } );

        return proc;

    }

}

export default VLC;