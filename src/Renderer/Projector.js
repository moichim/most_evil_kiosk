import Image from "./Screens/Image";
import Screensaver from "./Screens/Screensaver";
import Text from "./Screens/Text";
import Video from "./Screens/Video";

import "./Screens/Screen.scss";

/**
 * The main projector handler.
 * - bind controlling objects to the DOM from ./projector.html
 * - recieve() requests from the main.js
 */
class Projector {

    constructor() {
        
        this.app = document.getElementById( "projector" );
        
        this.screens = {
            image: new Image( "image" ),
            text: new Text( "text" ),
            video: new Video( "video" ),
            screensaver: new Screensaver( "screensaver" )
        }

    }

    /**
     * Process a request from the main.js
     * @param {Object} request 
     */
    recieve( request ) {

        console.log( request );

        for ( let key in this.screens ) {

            let screen = this.screens[ key ];

            if ( key === request.screen ) {
                screen.activate( request );
            } else {
                screen.deactivate();
            }

        }
    }

}

export default Projector;