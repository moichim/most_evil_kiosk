import Screen from "./Screen";

class Screensaver extends Screen {

    static defaults = {};

    static key = "screensaver";

    constructor( id ) {
        
        super( id );

        this.video = document.getElementById( "video-element" );

    }

    activate() {
        // this.setImage( data );
        super.activate();
    }

}

export default Screensaver;

Screensaver.name