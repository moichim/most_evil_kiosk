// import "./Screen.scss";

const ACTIVE_CLASS = "screen__active";

class Screen {

    static defaults =  {};
    static key = "";

    constructor( id ) {

        this.container = document.getElementById( id );
        this.active = false;

    }

    activate() {

        this.addClass( ACTIVE_CLASS );

    }

    deactivate() {

        this.removeClass( ACTIVE_CLASS );

    }

    addClass( cls ) {
        this.container.classList.add( cls );
    }

    removeClass( cls ) {
        this.container.classList.remove( cls );
    }

    toggleClass( cls ) {
        this.container.classList.toggle( cls );
    }

}

export default Screen;