import Screen from "./Screen";

class Text extends Screen {

    static defaults = {
        text: ""
    };

    static key = "text";

    constructor( id ) {
        
        super( id );

        this.text = document.getElementById( "text-element" );

    }

    activate() {
        // this.setImage( data );
        super.activate();
    }

    setContent( content = undefined ) {
        
        console.log( content );

    }

}

export default Text;