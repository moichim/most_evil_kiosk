import Screen from "./Screen";

class Video extends Screen {

    static defaults = {
        file: ""
    };

    static key = "video";

    constructor( id ) {
        
        super( id );

        this.wrapper = document.getElementById( "video-wrapper" );
        this.video = undefined;
        this.source = undefined;

    }

    activate( request ) {
        this.setVideo( request.file );
        super.activate();
    }

    setVideo( file ) {

        this.video = document.createElement( "video" );
        this.video.id = "video-element";

        this.source = document.createElement( "source" );
        this.source.id = "video-source";
        this.source.setAttribute( "type", "video/mp4" );
        this.source.src = file;

        this.video.appendChild( this.source );
        this.wrapper.appendChild( this.video );

        this.video.load();
        this.video.play();

        this.video.addEventListener( "ended", () => {

            this.source.remove();
            this.video.remove();
            
        } );

    }

    end() {

    }

}

export default Video;