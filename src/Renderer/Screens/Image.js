import Screen from "./Screen";

class Image extends Screen {

    static defaults = {
        file: ""
    };

    static key = "image";

    constructor( id ) {
        
        super( id );

        this.image = document.getElementById( "image-element" );

    }

    activate( request ) {
        this.setImage( request.file );
        super.activate();
    }

    setImage( path = undefined ) {
        
        if ( path === undefined) {
            this.image.src = "";
        } else {
            this.image.src = path;
        }

    }

}

export default Image;