import React from "react";

const { ipcRenderer } = window.require( "electron" );

const VLCTrigger = ( props ) => {

    props = Object.assign({
        title: "Spouštěč VLC",
        filename: "labir_edu_720.mp4"
    }, props );

    const handleClick = () => {

        ipcRenderer.send( "launch-vlc", "fajl" );

    }

    return(
        <button onClick={()=>handleClick()}>
            {props.title}
        </button>
    );

};

export default VLCTrigger;