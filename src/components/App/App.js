import React from "react";

import "./App.scss";

import VLCTrigger from "../VLCTrigger/VLCTrigger";
import IPCTrigger from "../IPCTrigger/IPCTrigger";



const App = () => {

    return(
        <div className="">

            <p>{process.version}</p>
        
            <div className="">
                <VLCTrigger />
            </div>

            <div className="">
                <IPCTrigger title="Spouštěč obrázku" channel="main-request" file="bloud.jpg" screen="image"/>
                <IPCTrigger title="Spouštěč videa" channel="main-request" file="labir_edu_720.mp4" screen="video"/>
                <IPCTrigger title="Spouštěč textu" channel="main-request" file="labir_edu_720.mp4" screen="text"/>
                <IPCTrigger title="Spouštěč screensaveru" channel="main-request" file="labir_edu_720.mp4" screen="screensaver"/>
            </div>
        
        </div>
    );
};

export default App;