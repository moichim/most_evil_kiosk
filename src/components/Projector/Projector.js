import React, { useState, useEffect } from "react";

import IPCTrigger from "../IPCTrigger/IPCTrigger";



const Projector = (props) => {

    const [ plays, setPlays ] = useState( 0 );

    return(
        <div className="">

            <p>{process.version}</p>

            <p>{props.plays ? "Hraji" : "nehraji"}</p>

            <div className="">
                <IPCTrigger title="Zastavovač videa" channel="stop-video" />
            </div>
        
        </div>
    );
};

export default Projector;