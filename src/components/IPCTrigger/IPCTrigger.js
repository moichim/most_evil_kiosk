import React from "react";
const { ipcRenderer } = window.require('electron')

const IPCTrigger = ( props ) => {

    props = Object.assign({
        title: "Spouštěč IPC",
        channel: "play-video"
    }, props );

    const handleClick = () => {

        ipcRenderer.send( props.channel, props );

    }

    return(
        <button onClick={()=>handleClick()}>
            {props.title}
        </button>
    );

};

export default IPCTrigger;